export let POSITION_INTERVAL = 5000; // 2000ms

export let DEAL_STATUS_PENDING = 'llamada';
export let DEAL_STATUS_ACCEPTED = 'accepted';
export let DEAL_TIMEOUT = 25; // 20s

export let TRIP_STATUS_WAITING = 'esperando';
export let TRIP_STATUS_GOING = 'yendo';
export let TRIP_STATUS_FINISHED = 'terminado';

export let TRANSACTION_TYPE_WITHDRAW = 'retirar';

export let DEFAULT_AVATAR = 'https://freeiconshop.com/wp-content/uploads/edd/person-outline-filled.png';
