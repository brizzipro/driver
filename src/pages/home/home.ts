import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import { DriverService } from '../../services/driver-service';
import { DealService } from "../../services/deal-service";
import { UserPage } from "../user/user";
import { AuthService } from "../../services/auth-service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  driver: any;
  deal: any;
  dealSubscription: any;

	llamada: any;
  constructor(public nav: NavController, public driverService: DriverService, public modalCtrl: ModalController,
              public alertCtrl: AlertController, public dealService: DealService, public authService: AuthService) {

    // get user info from service
    driverService.getDriver().take(1).subscribe(snapshot => {
      this.driver = snapshot;

      // if user did not complete registration, redirect to user setting
      if (this.driver.plate && this.driver.type) {
        this.watchDeals();
      } else {
        this.nav.setRoot(UserPage, {
          user: authService.getUserData()
        });
      }
    });
  }



  // make array with range is n
  range(n) {
    return new Array(Math.round(n));
  }

  // listen to deals
  watchDeals() {
    // listen to deals
    this.dealSubscription = this.dealService.getDeal(this.driver.$key).subscribe(snapshot => {
      this.deal = snapshot;
      if (snapshot.status == true) {

		this.llamada = true
      } else {
        this.llamada = false
      }
    });
  }
}
